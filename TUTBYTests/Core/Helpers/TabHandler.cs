﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TUTBYTests.DriverManager;

namespace TUTBYTests.Core.Helpers
{
    class TabHandler
    {
        public static void OpenNewTabAndSwitchToIt()
        {
            
            var driver = Driver.GetInstance().CurrentDriver;
            driver.SwitchTo().Window(Driver.GetInstance().CurrentDriver.WindowHandles.Last());
        }

        public static void CloseCurrentTabAndSwitchToMain()
        {
            var driver = Driver.GetInstance().CurrentDriver;
            driver.Close();
            driver.SwitchTo().Window(driver.WindowHandles.First());
        }

        public static string GetCurrentWindow()
        {
            var driver = Driver.GetInstance().CurrentDriver;
            var handle = driver.CurrentWindowHandle;

            return handle;

        }

        public List<string> GetWindowHandles()
        {
            var driver = Driver.GetInstance().CurrentDriver;

            List<string> handles = driver.WindowHandles.ToList();

            return handles;
        
        }

        
    }
}
