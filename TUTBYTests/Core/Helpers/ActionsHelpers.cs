﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Text;
using TUTBYTests.DriverManager;

namespace TUTBYTests.Core.Helpers
{
    public static class ActionsHelpers
    {
        public static void SelectElementByKeyboard()
        {
            var driver = Driver.GetInstance().CurrentDriver;

            Actions builder = new Actions(driver);

            builder
                .ClickAndHold()
                .SendKeys(Keys.PageDown)
                .SendKeys(Keys.PageDown)
                .Click()
                .Build()
                .Perform();

        }
    }
    
}
