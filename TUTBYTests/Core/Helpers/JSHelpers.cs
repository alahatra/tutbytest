﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using TUTBYTests.DriverManager;

namespace TUTBYTests.Core.Helpers
{
    static class JSHelpers
    {
        public static void SetAttribute()
        {
            var driver = Driver.GetInstance().CurrentDriver;
            ((IJavaScriptExecutor)driver).ExecuteScript("document.getElementsId('orb-search-q').setAttribute('value', 'New York')");

        }

        public static void ScrollToElement(By locator)
        {
            var driver = Driver.GetInstance().CurrentDriver;
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(false);", locator);
        }
    }
}
