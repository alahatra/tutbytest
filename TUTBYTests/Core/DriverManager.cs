﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace TUTBYTests.DriverManager
{
    public class Driver
    {
        private static readonly Lazy<Driver> lazy = new Lazy<Driver>(() => new Driver());

        private IWebDriver driver;
        public IWebDriver CurrentDriver => GetDriver("Chrome");

        private Driver()
        {

        }

        public IWebDriver GetDriver(string browser)
        {
            switch (browser)
            {
                case "Chrome":
                    if (driver == null)
                    {
                        driver = new ChromeDriver(Options());
                    }
                    return driver;
                case "Firefox":
                    if (driver == null)
                    {
                        driver = new FirefoxDriver();
                    }
                    return driver;
                case "InternetExplorer":
                    if (driver == null)
                    {
                        driver = new InternetExplorerDriver(IEOptions());
                    }
                    return driver;
      
            }
            return driver;
            
        }
        public static Driver GetInstance()
        {
            return lazy.Value;
        }

        public void QuitDriver()
        {
            if (driver == null)
            {
                return;
            }
            else
            {
                driver.Quit();
                driver = null;
            }
        }

        public ChromeOptions  Options()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("start-maximized");
            //options.AddArgument("--headless");
            options.AddArgument("disable-infobars");
            //options.AddUserProfilePreference("DownloadFolder",oject value);
            
            return options;
        }

        public InternetExplorerOptions IEOptions()
        {
            InternetExplorerOptions options = new InternetExplorerOptions();
            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            return options;
        }

        
       
    }
}
