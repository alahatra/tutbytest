﻿using System;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using TUTBYTests.DriverManager;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace TUTBYTests.Core
{
    public class Waits
    {
        IWebDriver InsDriver = Driver.GetInstance().CurrentDriver;


        public IWebElement WaitForExists(By locator)
        {
            DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(InsDriver);
            fluentWait.Timeout = TimeSpan.FromSeconds(10);
            fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
            fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return fluentWait.Until(ExpectedConditions.ElementExists(locator));
        }

        public IWebElement WaitForVisible(By locator)
        {
            DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(InsDriver);
            fluentWait.Timeout = TimeSpan.FromSeconds(10);
            fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
            fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return fluentWait.Until(ExpectedConditions.ElementIsVisible(locator));
        }

        public IWebElement WaitForClickable(By locator)
        {
            DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(InsDriver);
            fluentWait.Timeout = TimeSpan.FromSeconds(10);
            fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
            fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return fluentWait.Until(ExpectedConditions.ElementToBeClickable(locator));
        }

        public ReadOnlyCollection<IWebElement> WaitForVisibilityOfAllElements(By locator)
        {
            DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(InsDriver);
            fluentWait.Timeout = TimeSpan.FromSeconds(10);
            fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
            fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return fluentWait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(locator));
        }
    }
}
