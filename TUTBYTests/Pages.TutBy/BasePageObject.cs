﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using TUTBYTests.Core;
using TUTBYTests.DriverManager;
using OpenQA.Selenium.Support.UI;

namespace TUTBYTests.TexstsAndInputs
{
    public class BasePageObject : Waits
    {
        IWebDriver InsDriver = Driver.GetInstance().CurrentDriver;

         

        public List<string>GetListOfLinks(By locator) 
        {

            var listOfWebElements = WaitForVisibilityOfAllElements(locator);
          

            List<string> ListOfLinks = new List<string>();

            foreach (IWebElement element in listOfWebElements)
             {
                ListOfLinks.Add(element.GetAttribute("title"));
             }

            return ListOfLinks;

        }

        public void ClearSendKeys(By locator, string text)
        {
            WaitForVisible(locator).Clear();
            WaitForVisible(locator).SendKeys(text);

        }

        public void ButtonClick(By locator)
        {
            WaitForClickable(locator).Click();
        }

        public string GetAttribute(By locator, string attributeName)
        {
            return WaitForVisible(locator).GetAttribute(attributeName);
        }

        public string GetText(By locator)
        {
            return WaitForVisible(locator).Text;
        }

        public void SetUrl(string url)
        {
            InsDriver.Url = url;
        }

      
        public string GetUrl()
        {
            return InsDriver.Url;
        }

        public  void OpenInaNewTab(string url)
        {
            ((IJavaScriptExecutor)InsDriver).ExecuteScript("window.open('"+url+"')");
        }

        public  void OpenNewTabAndSwitchToIt(string url)
        {

            OpenInaNewTab(url);
            var driver = Driver.GetInstance().CurrentDriver;
            driver.SwitchTo().Window(Driver.GetInstance().CurrentDriver.WindowHandles.Last());
        }

        

        public static void CloseCurrentTabAndSwitchToMain()
        {
            var driver = Driver.GetInstance().CurrentDriver;
            driver.Close();
            driver.SwitchTo().Window(driver.WindowHandles.First());
        }

        public void ScrollToElement(By locator)
        {
            Actions action = new Actions(InsDriver);
            action.MoveToElement(WaitForExists(locator));
            
        }

        public void GoToFrame(string framename)
        {
            InsDriver.SwitchTo().Frame(framename);
        }

        

        public void ReturnFromFrame()
        {
            InsDriver.SwitchTo().DefaultContent();
        }

        /// <summary>
		/// could be used only if the dropdown has been a <select> element
		/// </summary>

        public void SelectItemFromDropDown(By locator, string name)
        {
            SelectElement dropdown = new SelectElement(WaitForClickable(locator));
            dropdown.SelectByText(name);
        }

        public void ClickItemsInDropDown(By locator)
        {
            WaitForVisible(locator);
         
        }

        
        

        


    }
}
