﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace TUTBYTests.TexstsAndInputs
{
    public class StartPageObject:BasePageObject
    {
        private readonly By _searchFieldLocator = By.Id("search_from_str");
        private readonly By _searchButtonLocator = By.Name("search");
        private readonly By _pageLanguageLocator = By.XPath("//a[contains(@href, 'https://www.tut.by/route/lang')]");
        private readonly By _currentLocationLocator = By.XPath("//span[contains(@class, 'region-ttl')]/span");
        private readonly By _newLocationLocator = By.XPath("//div[contains(text(), 'Витебская')]/following-sibling::*//a[contains(text(), 'Полоцк')]");

        private readonly By _newsLinkLocator = By.XPath("//li[contains(@class, 'topbar__li')]/a[contains(@title, 'Новости')]");
        private readonly By _mainMenuIconLocator = By.CssSelector("span.b-icon.icon-burger");
        private readonly By _allLinksInMainMenu = By.XPath("//div[@class='topbarmore-i']//a[contains(@href, '')]");
        private readonly By _closeButtonLocator = By.XPath("//span[contains(@class,'icon-close')]");

        private readonly By _searchDropDownLocator = By.XPath("//ul[@class='typeahead dropdown-menu']");
        private readonly By _firstElementInDropdown = By.XPath("//ul[@class='typeahead dropdown-menu']/li[1]");

        string attrinuteLinkTitle = "title";

        public void OpenStartPageInTwoTabs(string url)
        {
            OpenNewTabAndSwitchToIt(url);
            SetUrl(url);
        }

        public void EnterSearchTextToSearchField(string searchText)
        {
            ClearSendKeys(_searchFieldLocator, searchText);
        }

        public string GetInputValue(string value) //gets text input
        {
            GetAttribute(_searchFieldLocator, "value");
            return value;

        }

        public string GetLinkAttribute() //получает значение атрибута
        {
            return  GetAttribute(_pageLanguageLocator, attrinuteLinkTitle);
            
        }

        public void ClickSearchButton()
        {
            ButtonClick(_searchButtonLocator);
        }

        public string GetCurrentUrl()
        {
            return GetUrl();
        }

        public void ScrollToTheBottomOfThePage()
        {
            ScrollToElement(_pageLanguageLocator);
        }

        public void ClickSwitchLanguageLink()
        {
            ButtonClick(_pageLanguageLocator);
        }

        public void ClickLocationLink()
        {
            ButtonClick(_currentLocationLocator);
        }

        public void ClickNewLocationLink()
        {
            ButtonClick(_newLocationLocator);
        }

        public string GetLocationName()
        {
            return GetText(_currentLocationLocator);
        }

        public void OpenMainMenuTab()
        {
            ButtonClick(_mainMenuIconLocator);
        }

        public void ClickNewsLinkInMainMenu()
        {
            ButtonClick(_newsLinkLocator);
        }

        public List<string> GetLinksInMainMenu()
        {
            return GetListOfLinks(_allLinksInMainMenu);
            
        }

        public void CloseMainMenu()
        {
            ButtonClick(_closeButtonLocator);
        }

        public bool areListsEqual(List<string> firstList, List<string> secondList)
        {
            return firstList.TrueForAll(e => secondList.Contains(e));
        }

        public void SelectTheFirstItemFromDropDown()
        {
            ClickItemsInDropDown(_searchDropDownLocator);
            ButtonClick(_firstElementInDropdown);
        }
        

        public void RefreshPage(string url)
        {
            SetUrl(url);
        }
    }
}
