﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using TUTBYTests.TexstsAndInputs;

namespace TUTBYTests.Pages
{
   public class ToolbarMenuPageObject:BasePageObject
    {
        private readonly By _financeInfoButtonLocator = By.XPath("//li[@class='topbar__li']/a[contains(@title, 'Финансы')]");
        private readonly By _cultureNewLocator = By.XPath("//a[contains(@class, 'topbar__link') and contains(@title, 'Афиша')]");
        private readonly By _rabotaLocator = By.XPath("//a[contains(@class, 'topbar__link') and contains(@title, 'Работа')]");
        private readonly By _weatherLocator = By.XPath("//a[contains(@class, 'topbar__link') and contains(@title, 'Погода')]");
        private readonly By _tvSetLocator = By.XPath("//a[contains(@class, 'topbar__link') and contains(@title, 'ТV-программа')]");


        public void ClickFinanceNewsButton()
        {
            ButtonClick(_financeInfoButtonLocator);
        }

        public void ClickCultureNewsButton()
        {
            ButtonClick(_cultureNewLocator);
        }

        public void ClickRabotaButton()
        {
            ButtonClick(_rabotaLocator);
        }
         public void ClickWeatherLocator()
        {
            ButtonClick(_weatherLocator);
        }

        public void ClickTVSetButton()
        {
            ButtonClick(_tvSetLocator);
        }

        public string GetCurrentUrl()
        {
            return GetUrl();
        }

    }
}
