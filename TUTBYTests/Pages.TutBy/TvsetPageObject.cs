﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using TUTBYTests.TexstsAndInputs;

namespace TUTBYTests.Pages
{
    public class TvsetPageObject:BasePageObject
    {

        private readonly By _channelDropdownLocator = By.XPath("//select[@name='channel']");
        private readonly By _providerDropdownLocator = By.XPath("//select[@name='provider']");
        private readonly By _selectedChannelName = By.XPath("//select[@name='channel']/option[contains(text(), 'Кино')]");
        private readonly By _selectedProviderName = By.XPath("//select[@name='provider']/option[contains(text(), 'ZALA')]");

        string channelName = "Кино";
        string providerName = "ZALA";

        

        public void SelectChannel()
        {
            
            SelectItemFromDropDown(_channelDropdownLocator, channelName);
        }

        public void SelectProvider()
        {
            
            SelectItemFromDropDown(_providerDropdownLocator, providerName);
        }

        public string GetPageUrl()
        {
            return GetUrl();
        }

        public bool isChannelSelected(string _attribute)
        {
            string attribute = GetAttribute(_selectedChannelName, "selected");
            return attribute == _attribute;

            
        }

        public bool isProviderSelected(string _attribute)
        {
            string attribute = GetAttribute(_selectedProviderName, "selected");
            return attribute == _attribute;


        }

        public void RefreshPage(string url)
        {
            SetUrl(url);
        }




    }
}
