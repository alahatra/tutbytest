﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TUTBYTests.TexstsAndInputs
{
    public class SearchResultPageObject:BasePageObject
    {
        //private readonly By _firstLinkLocator = By.XPath("//li[contains(@class, 'b-results__li')]/h3/a[contains(@href, 'usd')]");
        private readonly By _firstLinkLocator = By.XPath("//li[contains(@class, 'b-results__li')]/h3/a[1]");

        private readonly By _LinkDescriptionLocator = By.XPath("//ol[contains(@class, 'results-list')]//p[contains(text(), '')]");

        public string GetCurrentUrl()
        {
            return GetUrl();
        }

        public void ClickTheFirstLinkOnThePage()
        {
            ButtonClick(_firstLinkLocator);
        }

        public string GetLinkDescriptionText()
        {
            return GetText(_LinkDescriptionLocator);
        }

        public void ClickTheLinkAndSwitchToNewPage()
        {
            ClickTheFirstLinkOnThePage();
            string linkUrl = GetCurrentUrl();
            OpenNewTabAndSwitchToIt(linkUrl);
        }
    }
}
