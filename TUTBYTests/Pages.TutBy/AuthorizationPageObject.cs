﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using TUTBYTests.TexstsAndInputs;



namespace TUTBYTests.TexstsAndInputs
{
    class AuthorizationPageObject:BasePageObject
    {
        StartPageObject startPage = new StartPageObject();

        private readonly By _enterButtonstartPageLocator = By.CssSelector("a.enter");
        private readonly By _userNameLocator = By.CssSelector("input[name=login]");
        private readonly By _passwordLocator = By.Name("password");
        private readonly By _loginButtonLocator = By.XPath("//div[@class='b-hold']/input[@value='Войти']");
        private readonly By _avatarLocator = By.XPath("//span[@class='auth-ava']//img[@class='ava']");
        private readonly By _logoutButton = By.XPath("//a[contains(text(), 'Выйти')]");
        private readonly By _loggedUserNameLocator = By.XPath("//span[@class='uname']");

        private readonly By _errorMessageLocator = By.XPath("//div[contains(@class, 'error-msg')]");
        
        
        public void ClickEnterButton()
        {
            ButtonClick(_enterButtonstartPageLocator);
        }
               
        public void TypeUserName(string username)
        {
            ClearSendKeys(_userNameLocator, username);

        }

        
        public void TypePassword(string password)
        {
            ClearSendKeys(_passwordLocator, password);
        }

        
        public void ClickLoginButton()
        {
            ButtonClick(_loginButtonLocator);
        }

        public void ClickLogoutButton()
        {
            ButtonClick(_logoutButton);
        }

        public void ClickAvatarIcon()
        {
            ButtonClick(_avatarLocator);
        }

        
        public string CheckUserIsLogged()
        {
            return GetText(_loggedUserNameLocator);
        }

        public bool IsUserLoggedOut(string expectedState)
        {
            return GetText(_enterButtonstartPageLocator) ==expectedState;
        }

        public string GetErrorMessageText()
        {
            return GetText(_errorMessageLocator);
        }

         
        public void LogOut()
        {
            ClickAvatarIcon();
            ClickLogoutButton();

        }

        

        

        


    }
}
