﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TUTBYTests.Pages.Yandex
{
    public class SortingAndFilteringPageObject:BasePageObject
    {
        private readonly By _actionCameraLocator = By.XPath("//div[contains(@data-zone-data, 'Экшн')]");
        private readonly By _fridgeMenuItemLocator = By.XPath("//div[@class='_1YdrMWBuYy']//a[contains(text(), 'Холодильники')]");
        
        private readonly By _sortingButtonLocator = By.XPath("//div[@class = 'b_2gZuXBaEU6']/button[@type='button']");
        private readonly By _expensiveFirstOptionLocator = By.XPath("//span[contains(text(), 'Сначала подороже')]");

        private readonly By _itemsInSortedListLocator = By.XPath("//div[@data-auto='price']//span[contains(text(), ' ')]");
        private readonly By _elem1Locator = By.XPath("//div[@class='b_lS7Xz3hU_W'][1]//span[@data-tid='c3eaad93'][1]");
        private readonly By _elem2Locator = By.XPath("//div[@class='b_lS7Xz3hU_W'][2]//span[@data-tid='c3eaad93'][1]");
        //private readonly By _itemsInSortedListLocator = By.XPath("//div[@class='b_3l6BMMP8pQ']//div[@class='cia-vs']");

        private readonly By _widthInputFieldLocator = By.XPath("//input[@name='Ширина до']");
        private readonly By _resultsHeaderLocator = By.XPath("//h1[@class='_3wPGpzKmmn']");

        
        
        public void SelectFridgeItemInMenu()
        {
            ButtonClick(_fridgeMenuItemLocator);
        }

        public void EnterMaxWidthOfFridge(string text)
        {
            ScrollToElement(_widthInputFieldLocator);
            ClearSendKeys(_widthInputFieldLocator, text);
        }

        
        public string GetFilteredListName()
        {
            WaitForPageToLoad();
            Thread.Sleep(1000);
            return GetText(_resultsHeaderLocator);
        }


        public void SelectActionCameraIcon()
        {
            ScrollToElement(_actionCameraLocator);
            ButtonClick(_actionCameraLocator);
        }

        public void SelectSortingOPtion()
        {
            ButtonClick(_sortingButtonLocator);
            ClickItemsInDropDown(_expensiveFirstOptionLocator);
        }

        public bool AreElemetsSorted()
        {
            
                 decimal el1 = decimal.Parse(GetText(_elem1Locator));
            
                decimal el2 = decimal.Parse(GetText(_elem2Locator));

            return el1 >= el2;
        }

    }
}
