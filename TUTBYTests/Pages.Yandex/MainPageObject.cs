﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TUTBYTests.Pages.Yandex
{
    public class MainPageObject:BasePageObject
    {

        private readonly By _marketIconLocator = By.XPath("//div[contains(text(), 'Маркет')]");
        private readonly By _musicIconLocator = By.XPath("//div[contains(text(), 'Музыка')]");
        private readonly By _skipSubscribeLocator = By.XPath("//div[contains(@class, 'popup')]//span[contains(@class, 'd-icon_cross-big')]");


        public void ClickNavigationIcon()
        {
            ButtonClick(_marketIconLocator);
            BasePageObject basePage = new BasePageObject();
            basePage.CloseCurrentTabAndSwitchToNew();
        }

        public void ClickMusicIcon()
        {
            ButtonClick(_musicIconLocator);
            BasePageObject basePage = new BasePageObject();
            basePage.CloseCurrentTabAndSwitchToNew();
            ButtonClick(_skipSubscribeLocator);
        }
    }
}
