﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace TUTBYTests.Pages.Yandex
{
    public class MarketPageObject:BasePageObject
    {
        private readonly By _searchFieldLocator = By.XPath("//input[@name='text']");
        private readonly By _searchButtonLocator = By.XPath("//button[@type='submit']");
        private readonly By _elektronikaLocator = By.XPath("//span[contains(text(), 'Электроника')]");
        private readonly By _homeAppliancesMenuLocator = By.XPath("//span[contains(text(), 'Бытовая техника')]");
        private readonly By _modalLocator = By.ClassName("_1guxbPKMJ6");
        private readonly By _confirmButtonLocator = By.XPath("//div[@class='_1guxbPKMJ6']//span[contains(text(), 'Понятно')]");

        private readonly By _firstItemInListLocator = By.XPath("//div[@data-zone-name='snippetList']/article[1]/div/a[contains(@href, 'note-8')]");
        private readonly By _secondItemInListLocator = By.XPath("//div[@data-zone-name='snippetList']/article[2]/div/a[contains(@href, 'note-8')]");
        private readonly By _compareIconLocator = By.ClassName("_1CXljk9rtd");
        private readonly By _comparisonListsLocator = By.XPath("//a[contains(@href, 'compare-lists')]/span[contains(text(), 'Сравнить')]");

        private readonly By _itemsInComparisonLocator = By.XPath("//div[@class='_3B3AAKx4qr']//a[contains(@href, 'note-8')]");
        private readonly By _removeComparisonListLocator = By.XPath("//button[@class='MOYcCv2eIJ _1KU3sPkiT1']");
        private readonly By _emptyComparisonListLocator = By.XPath("//h2[contains(text(), 'пока нечего')]");


        public void EnterTextToSearchField(string text)
        {
            ClearSendKeys(_searchFieldLocator, text);
        }

        public void ClickSearchButton()
        {
            ButtonClick(_searchButtonLocator);
        }

        public void AddToComparison()
        {
            ButtonClick(_compareIconLocator);
        }

        public void ClickCompareButton()
        {
            ButtonClick(_comparisonListsLocator);
        }

        public void ClickElekronikaSectionIcon()
        {
            //if (IsElementDisplayed(_modalLocator))
            //{
            //    ButtonClick(_confirmButtonLocator);
            //    ButtonClick(_elektronikaLocator);
            //}
            //else
            //{
            //    ButtonClick(_elektronikaLocator);
            //}

            ButtonClick(_elektronikaLocator);
        }

        public void ClickHomeAppliancesMenuIcon()
        {
            //if (IsElementDisplayed(_modalLocator))
            //{
            //    ButtonClick(_confirmButtonLocator);
            //    ButtonClick(_homeAppliancesMenuLocator);
            //}
            //else
            //{
            //    ButtonClick(_homeAppliancesMenuLocator);
            //}

            ButtonClick(_homeAppliancesMenuLocator);

        }

        public void SeletTheFirstItemInList()
        {
            ButtonClick(_firstItemInListLocator);
            var basePage = new BasePageObject();
            basePage.SwitchToLastTab();
        }

        public void SelectTheSecondItemInList()
        {
            ButtonClick(_secondItemInListLocator);
            var basePage = new BasePageObject();
            basePage.SwitchToLastTab();

        }

        public void DeleteAllItemsFromComparison()
        {
            ButtonClick(_removeComparisonListLocator);
        }

        public List<string> GetItemsInComparisonMenu()
        {
            return GetListOfLinks(_itemsInComparisonLocator);
        }

        public bool IsElementContainedInList(List<string> elements, string item)
        {
            elements = GetItemsInComparisonMenu();
            return elements.Any(el => el.Contains(item)); 
            
        }

        public bool IsInformationTextDisplayed(string text)
        {
            return GetText(_emptyComparisonListLocator) == text;
        }
    }
}
