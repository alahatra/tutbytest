﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TUTBYTests.DriverManager;

namespace TUTBYTests.Pages.Yandex
{
    public class MusicPageObject:BasePageObject
    {
        private readonly By _searchFieldLocator = By.XPath("//input[@type='text']");
        private readonly By _skipSubscribeLocator = By.XPath("//div[contains(@class, 'payment-plus')]//span[contains(@class, 'd-icon_cross-big')]");


        private readonly By _metallicaArtistInListLocator = By.XPath("//a[contains(@href, '3121')]");
        private readonly By _beyonceArtistInListLocator = By.XPath("//div[contains(@class, 'suggest__item')]//a[contains(@href, '27995')]");
        private readonly By _displayedArtistLocator = By.XPath("//div[contains(@class, 'page-artist__stamp')]/following-sibling::h1");
        
        private readonly By _firstTrackInListLocator = By.XPath("//div[@class='d-track__overflowable-column']");
        private readonly By first = By.XPath("//div[contains(@class, 'controls')]//button[contains(@class, 'button')]//span[contains(@class, 'play')]");


        private readonly By _playIconLocator = By.XPath("//div[@class='d-icon d-icon_play']");
        private readonly By _pauseIconLocator = By.XPath("//div[contains(@class, 'd-icon d-icon_play')]/parent::div[contains(@class, 'btn_play')]");
       



        public void EnterArtistToSearchField(string text)
        {
            ClearSendKeys(_searchFieldLocator, text);
        }

        public void SelectMetallicaFromDropdown()
        {
            ButtonClick(_metallicaArtistInListLocator);
        }

        public bool IsArtistDisplayed(string artistName)
        {
            string displayedArtist = GetText(_displayedArtistLocator);
            return displayedArtist == artistName;
        }

        public void SelectBeyonceFromDropDown()
        {
            ButtonClick(_beyonceArtistInListLocator);
        }

        public void PlayTheFirstTrackInPopularTracks()
        {
            var driver = Driver.GetInstance().CurrentDriver;

            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,400)", "");


            WaitForPageToLoad();
            //Thread.Sleep(1000);
            ButtonClick(_firstTrackInListLocator);
            
            ScrollToElement(first);
            ButtonClick(first);
           
        }

        public bool IsTrackInProgress()
        {
           string attribute =  GetAttribute(_pauseIconLocator, "title");

            return attribute == "Пауза [P]";
        
        }

        public void StopPlaying()
        {
            ButtonClick(_playIconLocator);
        }

         public bool IsTrackStopped()
        {
            string attribute = GetAttribute(_pauseIconLocator, "title");

            return attribute == "Играть [P]";

        }
    }
}
