﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TUTBYTests.Pages.Yandex
{
   public class LoginPageObject:BasePageObject
    {
        private readonly By _loginButtonLocator = By.XPath("//a[contains(@class, 'login-enter')]");
        private readonly By _userNameLocaotr = By.XPath("//input[@data-t='field:input-login']");
        private readonly By _submitLocaotr = By.XPath("//button[@type='submit']");
        private readonly By _passwordLocator = By.XPath("//input[@data-t='field:input-passwd']");


        private string userName = "alahatra";
        private string password = "alahatra123";

        public void Authorization()
        {
            ButtonClick(_loginButtonLocator);
            SwitchToLastTab();
            ClearSendKeys(_userNameLocaotr, userName);
            ButtonClick(_submitLocaotr);
            ClearSendKeys(_passwordLocator, password);
            ButtonClick(_submitLocaotr);
        }
    }
}
