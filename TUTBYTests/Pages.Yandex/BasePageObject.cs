﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using TUTBYTests.Core;
using TUTBYTests.DriverManager;
using System.Linq;

namespace TUTBYTests.Pages.Yandex
{
   public class BasePageObject:Waits
    {
        IWebDriver InsDriver = Driver.GetInstance().CurrentDriver;


        public void WaitForPageToLoad()
        {
            new WebDriverWait(InsDriver, TimeSpan.FromSeconds(10)).Until(d => ((IJavaScriptExecutor)InsDriver).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public List<string> GetListOfLinks(By locator)
        {

            var listOfWebElements = WaitForVisibilityOfAllElements(locator);


            List<string> ListOfLinks = new List<string>();

            foreach (IWebElement element in listOfWebElements)
            {
                ListOfLinks.Add(element.GetAttribute("title"));
            }

            return ListOfLinks;

        }

        public List<string> GetListOfAttributes(By locator)
        {
            var listOfWebAttributes = WaitForVisibilityOfAllElements(locator);


            List<string> ListOfAttributes = new List<string>();

            foreach (IWebElement element in listOfWebAttributes)
            {
                ListOfAttributes.Add(element.Text);
            }

            return ListOfAttributes;
        }

        public void ClearSendKeys(By locator, string text)
        {
            WaitForVisible(locator).Clear();
            WaitForVisible(locator).SendKeys(text);

        }

        public void ButtonClick(By locator)
        {
            WaitForClickable(locator).Click();

        }

        public void ClickHiddenIcon(By locator)
        {
            WaitForExists(locator);
        }

        public string GetAttribute(By locator, string attributeName)
        {
            return WaitForVisible(locator).GetAttribute(attributeName);
        }

        public string GetText(By locator)
        {
            return WaitForVisible(locator).Text;
        }

        public void SetUrl(string url)
        {
            InsDriver.Url = url;
        }


        public string GetUrl()
        {
            return InsDriver.Url;
        }

        

        public void ScrollToElement(By locator)
        {
            Actions action = new Actions(InsDriver);
            action.MoveToElement(WaitForExists(locator));
        }

        public void GoToFrame(string framename)
        {
            InsDriver.SwitchTo().Frame(framename);
        }



        public void ReturnFromFrame()
        {
            InsDriver.SwitchTo().DefaultContent();
        }

        /// <summary>
		/// could be used only if the dropdown has been a <select> element
		/// </summary>

        public void SelectItemFromDropDown(By locator, string name)
        {
            SelectElement dropdown = new SelectElement(WaitForClickable(locator));
            dropdown.SelectByText(name);
        }

        public void ClickItemsInDropDown(By locator)
        {
            WaitForVisible(locator);

        }

        public void OpenInaNewTab(string url)
        {
            ((IJavaScriptExecutor)InsDriver).ExecuteScript("window.open('" + url + "')");
        }

       

        public  void CloseCurrentTabAndSwitchToMain()
        {
            var driver = Driver.GetInstance().CurrentDriver;
            driver.Close();
            driver.SwitchTo().Window(driver.WindowHandles.First());
        }

        public void CloseCurrentTabAndSwitchToNew()
        {
            var driver = Driver.GetInstance().CurrentDriver;
            driver.Close();
            driver.SwitchTo().Window(driver.WindowHandles.Last());

        }

        public void SwitchToLastTab()
        {
            var driver = Driver.GetInstance().CurrentDriver;
            driver.SwitchTo().Window(driver.WindowHandles.Last());

        }

        
        public bool IsElementDisplayed(By locator)
        {
            return WaitForVisible(locator).Displayed;
        }

        


    }
}
