﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TUTBYTests.DriverManager;
using TUTBYTests.Pages;
using TUTBYTests.TexstsAndInputs;

namespace TUTBYTests.Tests
{
    public class TVsetTests:BaseTest
    {
        ToolbarMenuPageObject toolBar = new ToolbarMenuPageObject();
        TvsetPageObject tvSet = new TvsetPageObject();
        string tvSetUrl = "https://tvset.tut.by/";

        [Test]

        public void OpenChannelProgram()
        {
            toolBar.ClickTVSetButton();
            Assert.IsTrue(tvSet.GetPageUrl().Contains(tvSetUrl), tvSetUrl, "Wrong page is displayed");
            tvSet.SelectChannel();
            Assert.IsTrue(tvSet.isChannelSelected("true"), "element is not selected");
            tvSet.SelectProvider();
            Assert.IsTrue(tvSet.isProviderSelected("true"), "element is not selected");
            

        }
    }
}
