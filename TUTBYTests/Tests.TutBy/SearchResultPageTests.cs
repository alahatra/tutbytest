﻿using System;
using System.Collections.Generic;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using TUTBYTests.DriverManager;
using TUTBYTests.TexstsAndInputs;

namespace TUTBYTests.Tests
{
    [TestFixture]
    [AllureNUnit]
    [AllureSuite("SearchTests")]
    class SearchResultPageTests:BaseTest
    {
        StartPageObject startPage = new StartPageObject();
        SearchResultPageObject resultPage = new SearchResultPageObject();

        string searchText = "Лукашенко";
        string resultPageUrl = "https://search.tut.by/";

        [Test]

        public void SearchResultsPageIsDisplayed()
        {
            startPage.EnterSearchTextToSearchField(searchText);
            startPage.ClickSearchButton();
            string searchPageUrl = resultPage.GetCurrentUrl();
            Assert.IsTrue(searchPageUrl.Contains(resultPageUrl), searchPageUrl, "Wrong page is displayed");

        }

        [Test]

        public void ClickTheFirstLinkOnThePage()
        {
            startPage.EnterSearchTextToSearchField(searchText);
            startPage.ClickSearchButton();
            resultPage.ClickTheLinkAndSwitchToNewPage();
            string urlAfterClick = resultPage.GetCurrentUrl();
            Assert.IsFalse(urlAfterClick.Contains(resultPageUrl), "Link hasn't been clicked");
        }


    }
}
