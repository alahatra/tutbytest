﻿using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TUTBYTests.DriverManager;
using TUTBYTests.Pages;

namespace TUTBYTests.Tests
{
    [TestFixture]
    [AllureNUnit]
    [AllureSuite("Navigation Tests ")]
    public class NavigationToolBarTests:BaseTest
    {
        ToolbarMenuPageObject toolbar = new ToolbarMenuPageObject();

        string financePageUrl = "https://finance.tut.by/";
        string afishaPageUrl = "https://afisha.tut.by/";
        string rabotaPageUrl = "https://rabota.by/";
        string pogodaPageUrl = "https://pogoda.tut.by/";

        [Test]

        public void FinancePageIsDisplayedTest()
        {
            toolbar.ClickFinanceNewsButton();
            string currentUrl = toolbar.GetCurrentUrl();
            Assert.IsTrue(currentUrl.Contains(financePageUrl), financePageUrl, "wrong page is displayed");
        }

        [Test]

        public void AfishaPageIsDisplayedTest()
        {
            toolbar.ClickCultureNewsButton();
            string currentUrl = toolbar.GetCurrentUrl();
            Assert.IsTrue(currentUrl.Contains(afishaPageUrl), afishaPageUrl, "wrong page is displayed");
        }

        [Test]

        public void RabotaPageIsDisplayedTest()
        {
            toolbar.ClickRabotaButton();
            string currentUrl = toolbar.GetCurrentUrl();
            Assert.IsTrue(currentUrl.Contains(rabotaPageUrl), rabotaPageUrl, "wrong page is displayed");
        }

        [Test]

        public void WeatherPageIsDisplayedTest()
        { 
            toolbar.ClickWeatherLocator();
            string currentUrl = toolbar.GetCurrentUrl();
            Assert.IsTrue(currentUrl.Contains(pogodaPageUrl), pogodaPageUrl, "wrong page is displayed");
        }
    }
}
