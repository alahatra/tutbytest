using Allure.Commons;
using NUnit.Allure.Core;
using NUnit.Framework;
using System;
using TUTBYTests.TexstsAndInputs;

namespace TUTBYTests.DriverManager
{
    
    public class BaseTest
    {
        protected Driver driver = Driver.GetInstance();

        protected BasePageObject basePage = new BasePageObject();
        string mainUrl = "https://www.tut.by/";

        [OneTimeSetUp]

        public void CleanReportDirectory()
        {
            AllureLifecycle.Instance.CleanupResultDirectory();
            Console.WriteLine("Set up has started");
        }
        

        [SetUp]
        public void OpenBrowser()
        {
           

            basePage.SetUrl(mainUrl);
            
        }

        [OneTimeTearDown]

        public void TearDown()
        {
            driver.QuitDriver();
        }
    }
}