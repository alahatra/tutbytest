﻿using OpenQA.Selenium;
using NUnit.Framework;
using TUTBYTests.DriverManager;
using TUTBYTests.TexstsAndInputs;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NUnit.Allure.Core;
using NUnit.Allure.Attributes;
using Allure.Commons;
using System;

namespace TUTBYTests.Tests
{
    [TestFixture]
    [AllureNUnit]
    [AllureSuite("StartPageTests")]

    public class StartPageTests:BaseTest
    {
        StartPageObject startPage = new StartPageObject();
        SearchResultPageObject resultPage = new SearchResultPageObject();
            
        string searchText = "Лукашенко";
        string url = "https://www.tut.by/";
        string pageLanguageVersion = "По-русски";
        string newPageLocation = "Полоцк";

        
        [Test, Order(1)]
        
        [AllureStory("haha")]    
        public void OpenStartPageInTwoTabsTest()
        {
            

            startPage.OpenStartPageInTwoTabs(url);
            string newTab = startPage.GetCurrentUrl();
            AllureLifecycle.Instance.WrapInStep(() =>
            {
                Console.WriteLine("Test is finished");
                Assert.AreEqual(newTab, url, "It's not a duplicate page");
            }, "Final");
        }

        [Test, Order(2)]
        
        public void VerifyTextInSearchFieldTest()
        {
            startPage.EnterSearchTextToSearchField(searchText);
            startPage.GetInputValue(searchText);
            Assert.AreEqual(searchText, startPage.GetInputValue(searchText), "Text is not displayed");
        }

        [Test]
        
        public void ChangeLanguageOfThePageTest()
        {
            startPage.ScrollToTheBottomOfThePage();
            startPage.ClickSwitchLanguageLink();
            
            startPage.ScrollToTheBottomOfThePage();
            string linkTitle = startPage.GetLinkAttribute();
            Assert.AreEqual(linkTitle, pageLanguageVersion, "Wrong page version");
        
        }

        [Test, Order(3)]
        
        public void ChangeLocationTest()
        {
            startPage.ClickLocationLink();
            startPage.ClickNewLocationLink();
            startPage.SetUrl(url);
            string locationText = startPage.GetLocationName();

            Assert.AreEqual(newPageLocation, locationText, "Location hasn't changed");
        }

        [Test, Order(5)]
       
        public void SelectTheFirstItemfromDropDownTest()
        {
            startPage.EnterSearchTextToSearchField(searchText);
            startPage.SelectTheFirstItemFromDropDown();
            resultPage.GetLinkDescriptionText();
            Assert.IsTrue(resultPage.GetLinkDescriptionText().Contains(searchText), "False results on the page");
           
        }

        [Test, Order(4)]
        
        public void MenuOptionsAreEqualInDifferentLocationsTest()
        {
            startPage.OpenMainMenuTab();

            List<string> firstList = startPage.GetLinksInMainMenu();
            startPage.CloseMainMenu();
            startPage.ClickLocationLink();
            startPage.ClickNewLocationLink();
            startPage.RefreshPage(url);
            startPage.OpenMainMenuTab();
            List<string> secondList = startPage.GetLinksInMainMenu();
            Assert.IsTrue(startPage.areListsEqual(firstList, secondList));
                      

        }
    }
}
