﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using NUnit.Framework;
using TUTBYTests.DriverManager;
using TUTBYTests.TexstsAndInputs;
using NUnit.Allure.Core;

namespace TUTBYTests.Tests
{
    [TestFixture]
    [AllureNUnit]

    public class AuthorizationTests:BaseTest
    {
        

        AuthorizationPageObject login = new AuthorizationPageObject();
        
        string loggedUserName = "Test Test";
        string errorMessage = "Неверное имя пользователя или пароль";

        

        [Test]

        public void LogInWithCorrectDataTest()
        {
            login.ClickEnterButton();
            login.TypeUserName(TexstsAndInputs.LoginInputs.validLoginName);
            login.TypePassword(TexstsAndInputs.LoginInputs.validPassword);
            login.ClickLoginButton();
            string actualLoggedUser = login.CheckUserIsLogged();
            Assert.AreEqual(loggedUserName, actualLoggedUser, "User is not logged");
            login.LogOut();
            
        }

        [Test]

        public void LogOutTest()
        {
            login.ClickEnterButton();
            login.TypeUserName(TexstsAndInputs.LoginInputs.validLoginName);
            login.TypePassword(TexstsAndInputs.LoginInputs.validPassword);
            login.ClickLoginButton();
            login.LogOut();
            Assert.IsTrue(login.IsUserLoggedOut("Войти"), "user is not logged out");
        }

        [Test]

        public void LogInWithIncorrectUserNameTest()
        {
            login.ClickEnterButton();
            login.TypeUserName(TexstsAndInputs.LoginInputs.invalidLoginName);
            login.TypePassword(TexstsAndInputs.LoginInputs.validPassword);
            login.ClickLoginButton();
            Assert.AreEqual(login.GetErrorMessageText(), errorMessage, "Message is not displayed");
            
            
        }

        [Test]

        public void LogInWithWrongPasswordTest()
        {
            login.ClickEnterButton();
            login.TypeUserName(TexstsAndInputs.LoginInputs.validLoginName);
            login.TypePassword(TexstsAndInputs.LoginInputs.invalidPassword);
            login.ClickLoginButton();
            Assert.AreEqual(login.GetErrorMessageText(), errorMessage, "Message is not displayed");
            
        }

        [Test, Description("Test to practice value attribute")]
        

        public void CorrectAuthorization([Values("alahatra123")] string loginName, [Values("alahatra123")] string password)
        {
            login.ClickEnterButton();
            login.TypeUserName(loginName);
            login.TypePassword(password);
            login.ClickLoginButton();
            string actualLoggedUser = login.CheckUserIsLogged();
            Assert.AreEqual(loggedUserName, actualLoggedUser, "User is not logged");
            login.LogOut();
        }

        [Test, Description("Test to practice value attribute")]
        
        public void IncorrectAuthorization([Values("alahatra", "alla")] string loginName, [Values("alahatra", "alla")] string password)
        {
            login.ClickEnterButton();
            login.TypeUserName(loginName);
            login.TypePassword(password);
            login.ClickLoginButton();
            Assert.AreEqual(login.GetErrorMessageText(), errorMessage, "Message is not displayed");
            
        }







    }
}
