﻿using System;
using System.Collections.Generic;
using System.Text;
using TUTBYTests.DriverManager;
using TUTBYTests.Pages.Yandex;
using NUnit.Framework;
using NUnit.Allure.Core;
using Allure.Commons;

namespace TUTBYTests.Tests.Yandex
{
    
    public class BaseTest
    {
        protected Driver driver = Driver.GetInstance();

        protected BasePageObject basePage = new BasePageObject();
        string mainUrl = "https://www.yandex.by/";

        [SetUp]

        public void OpenBrowser()
        {
            basePage.SetUrl(mainUrl);
        }

        [OneTimeTearDown]

        public void CloseBrowser()
        {
            driver.QuitDriver();
        }
    }
}
