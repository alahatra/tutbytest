﻿using NUnit.Allure.Core;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TUTBYTests.Pages.Yandex;

namespace TUTBYTests.Tests.Yandex
{
    [TestFixture]
    [AllureNUnit]

    public class ComparisonTests : BaseTest
    {
        MarketPageObject marketPage = new MarketPageObject();
        MainPageObject mainPage = new MainPageObject();
        BasePageObject basePage = new BasePageObject();

        string informationText = "Сравнивать пока нечего";
        string searchItem = "Note 8";

        [Test]
        public void ElementsShouldBeDeletedFromComparisonTest()
        {
            mainPage.ClickNavigationIcon();
            marketPage.EnterTextToSearchField(searchItem);
            marketPage.ClickSearchButton();
            marketPage.SeletTheFirstItemInList();
            marketPage.AddToComparison();
            basePage.CloseCurrentTabAndSwitchToMain();
            marketPage.SelectTheSecondItemInList();
            marketPage.AddToComparison();
            Thread.Sleep(1000);
            marketPage.ClickCompareButton();
            marketPage.GetItemsInComparisonMenu();
            List<string> items = marketPage.GetItemsInComparisonMenu();
            marketPage.DeleteAllItemsFromComparison();
            Assert.IsTrue(marketPage.IsInformationTextDisplayed(informationText), "page is not empty");


        }

        [Test]

        public void VerifyTharAddedItemsAreDisplayedInComparisonTest()
        {
            mainPage.ClickNavigationIcon();
            marketPage.EnterTextToSearchField(searchItem);
            marketPage.ClickSearchButton();
            marketPage.SeletTheFirstItemInList();
            marketPage.AddToComparison();
            basePage.CloseCurrentTabAndSwitchToMain();
            marketPage.SelectTheSecondItemInList();
            marketPage.AddToComparison();
            Thread.Sleep(1000);
            marketPage.ClickCompareButton();
            marketPage.GetItemsInComparisonMenu();
            List<string> items = marketPage.GetItemsInComparisonMenu();
            Assert.IsTrue(marketPage.IsElementContainedInList(items, "Note"), "wrong list is shown");
        }
    }
}
