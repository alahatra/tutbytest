﻿using System;
using System.Collections.Generic;
using System.Text;
using Allure.Commons;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using TUTBYTests.Pages.Yandex;

namespace TUTBYTests.Tests.Yandex
{
    [TestFixture]
    [AllureNUnit]
    [AllureSuite("MusicPageTests")]
    public class MusicTests:BaseTestWithLogin
    {
        MainPageObject mainPage = new MainPageObject();
        LoginPageObject login = new LoginPageObject();
        MusicPageObject musicPage = new MusicPageObject();


        
        [Test(Author = "Olga")]
        [Category("Login")]
        [Description("LogInTest")]
        [AllureTag("Nunit", "Test")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureFeature("Core")]

        public void IsMetallicaSelectedTest()
        {
            basePage.SetUrl("https://yandex.by/");
            mainPage.ClickMusicIcon();
            
            musicPage.EnterArtistToSearchField("Metallica");
            musicPage.SelectMetallicaFromDropdown();
            
            Assert.IsTrue(musicPage.IsArtistDisplayed("Metallica"), "error");


        }

        [Test]

        public void VerifyThatMusicIsPlayingTest()
        {
            basePage.SetUrl("https://yandex.by/");
            mainPage.ClickMusicIcon();
            musicPage.EnterArtistToSearchField("beyo");
            musicPage.SelectBeyonceFromDropDown();
            musicPage.PlayTheFirstTrackInPopularTracks();
            Assert.IsTrue(musicPage.IsTrackInProgress(), "Track is not in progress");
            musicPage.StopPlaying();
            Assert.IsTrue(musicPage.IsTrackStopped(), "Track is still playing");


        }

    }
}
