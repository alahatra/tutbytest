﻿using Allure.Commons;
using NUnit.Allure.Core;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TUTBYTests.DriverManager;
using TUTBYTests.Pages.Yandex;

namespace TUTBYTests.Tests.Yandex
{
    [TestFixture]
    [AllureNUnit]

    public class BaseTestWithLogin
    {
        protected Driver driver = Driver.GetInstance();

        protected BasePageObject basePage = new BasePageObject();
        LoginPageObject login = new LoginPageObject();
        string mainUrl = "https://www.yandex.by/";

        
        [OneTimeSetUp]
        public void OpenBrowser()
        {
            basePage.SetUrl(mainUrl);

            login.Authorization();


        }

        [OneTimeTearDown]

        public void TearDown()
        {
            driver.QuitDriver();
        }
    }
}
