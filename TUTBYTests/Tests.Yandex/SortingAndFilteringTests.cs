﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TUTBYTests.Pages.Yandex;

namespace TUTBYTests.Tests.Yandex
{
    
    public class SortingAndFilteringTests:BaseTest
    {
        MainPageObject mainPage = new MainPageObject();
        MarketPageObject marketPage = new MarketPageObject();
        SortingAndFilteringPageObject sortAndFilter = new SortingAndFilteringPageObject();

        [Test]
        [Ignore ("need to fix dropdown")]

        public void ItemsAreSortedByPriceTest()
          {
            mainPage.ClickNavigationIcon();
            marketPage.ClickElekronikaSectionIcon();
            sortAndFilter.SelectActionCameraIcon();
            sortAndFilter.SelectSortingOPtion();
            //List<string> prices = sortAndFilter.GetPrices();

            Assert.IsTrue(sortAndFilter.AreElemetsSorted(), "loser");
            

        }

        [Test]
        public void ItemsAreSortedByWidthParameterTest()
        {
            mainPage.ClickNavigationIcon();
            marketPage.ClickHomeAppliancesMenuIcon();
            sortAndFilter.SelectFridgeItemInMenu();
            sortAndFilter.EnterMaxWidthOfFridge("50");
            //Assert.IsTrue(sortAndFilter.GetFilteredListName().Contains("до 50 см"), "not sorted");
            //Assert.IsTrue(sortAndFilter.AreFridgesSorted(), "not sorted");
            Assert.AreEqual(sortAndFilter.GetFilteredListName(), "Холодильники шириной до 50 см в Минске", "not sorted");
        }


    }
}
